libu2f-host (1.1.6-1) unstable; urgency=medium

  * New upstream version 1.1.6 (2018-05-15)

  * Autogenerate AppStream metadata
  * Split the documentation in a separate package
  * Move the packaging repository to salsa.d.o
  * Use the tracker.debian.org email address for the maintainers
  * libu2f-udev: Reload and apply rules when installing the package.
    Closes: #899299

  * Update debian/copyright
  * debian/control
    + Set Rules-Requires-Root to no
    + Declare compliance with policy v4.1.5
  * debian/rules
    + Simplify
    + Add a linting rule for Debian-specific scripts
    + Make dh_missing fail the build
      - Add debian/not-installed to exclude files
      - Install built HTML documentation rather than the source version

 -- Nicolas Braud-Santoni <nicolas@braud-santoni.eu>  Tue, 24 Jul 2018 19:33:22 +0800

libu2f-host (1.1.5-1) unstable; urgency=medium

  * New upstream version 1.1.5

  * Fix error in changelog for v1.1.4-1
  * Comply with Debian policy version 4.1.3
    * debian/copyright: Use HTTPS for Format URI
  * Switch to debhelper 11
  * Remove obsolete debian/libu2f-common.install

 -- Nicolas Braud-Santoni <nicolas@braud-santoni.eu>  Sat, 17 Mar 2018 02:57:57 +0100

libu2f-host (1.1.4-1) unstable; urgency=medium

  * New upstream release (2017-09-01)
    * Support for more devices in the udev rules.
    * Support larger certificates.
  * debian/control
    * Build-Depends on udev on linux-any. (Closes: #846358)
    * Convert to Multi-Arch
  * Packaging cleanups
    * Switch to debhelper 10, making dh-autoreconf unnecessary
    * Replace `Priority: extra` with `optional`
    * Update copyright on the Debian packaging
  * Split udev rules to libu2f-udev. (Closes: #846359, #824532)
  * Patch away the dependency on gtkdoc-mktmpl. (Closes: #876579)

 -- Nicolas Braud-Santoni <nicolas@braud-santoni.eu>  Mon, 30 Oct 2017 08:56:18 +0100

libu2f-host (1.1.3-1) unstable; urgency=medium

  * New upstream version 1.1.3.
  * Bump Standards-Version.

 -- Simon Josefsson <simon@josefsson.org>  Fri, 18 Aug 2017 20:09:57 +0200

libu2f-host (1.1.2-2) unstable; urgency=medium

  * debian/control: Move the packaging repo to Alioth
  * debian/control: Add myself as uploader

 -- Nicolas Braud-Santoni <nicolas@braud-santoni.eu>  Fri, 23 Sep 2016 20:42:49 +0200

libu2f-host (1.1.2-1) unstable; urgency=medium

  * Acknowledge NMU, thanks Nicolas.

 -- Simon Josefsson <simon@josefsson.org>  Wed, 10 Aug 2016 16:39:42 +0200

libu2f-host (1.1.2-0.2) unstable; urgency=medium

  * Non-maintainer upload.
  * Fix symbols files for previous upload.

 -- Gianfranco Costamagna <locutusofborg@debian.org>  Wed, 06 Jul 2016 12:36:25 +0200

libu2f-host (1.1.2-0.1) unstable; urgency=medium

  * Non-maintainer upload.
  * New upstream version 1.1.2 (2016-06-22)
  * Use compile-time hardening
  * Use HTTPS for Vcs-Git
  * Update and fix the symbols file.
  * Fix the copyright file.

 -- Nicolas Braud-Santoni <nicolas@braud-santoni.eu>  Thu, 30 Jun 2016 18:06:05 +0200

libu2f-host (1.0.0-1.2) unstable; urgency=medium

  * Non-maintainer upload.
  * Fix dependencies (glib2.0) (Closes: #820686).
  * Bump Standards-Version.

 -- Nicolas Braud-Santoni <nicolas@braud-santoni.eu>  Wed, 01 Jun 2016 23:55:05 +0200

libu2f-host (1.0.0-1.1) unstable; urgency=medium

  * Non-maintainer upload.
  * Switch from libjson0-dev to libjson-c-dev (Closes: #815413)

 -- Neil Williams <codehelp@debian.org>  Sun, 21 Feb 2016 11:52:27 +0000

libu2f-host (1.0.0-1) unstable; urgency=low

  * New upstream version.
  * Tweak watch file.
  * Update upstream-signing-key.pgp.
  * Update README.source.

 -- Simon Josefsson <simon@josefsson.org>  Tue, 22 Sep 2015 08:45:55 +0200

libu2f-host (0.0.4-1) unstable; urgency=low

  * Initial release.  Closes: #764262.

 -- Klas Lindfors <klas@yubico.com>  Thu, 22 Jan 2015 21:06:14 +0100
